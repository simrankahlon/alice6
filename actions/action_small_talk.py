from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from rasa_sdk import Action
from rasa_sdk.events import SlotSet
import json
from rasa_sdk.events import UserUtteranceReverted
import os
from env import *
from datetime import datetime
from rasa_sdk.events import SlotSet, Form
from lib.get_responses_from_db import fetch_faq_response_api, format_api_response
from lib import slotStorage
import logging

class ActionSmallTalk(Action):
	def name(self):
		return 'action_small_talk'
		
	def run(self, dispatcher, tracker, domain):
		try:
			intent_name = tracker.latest_message["intent"]["name"]
			sender_id = tracker.sender_id
			group_id = slotStorage.get_slot(sender_id,"group_id")
			platform = slotStorage.get_slot(sender_id,"platform")
			entity_name = None
			entity_value = None
			post_action = None
			post_data = None
			if tracker.latest_message["entities"]:
				for entity in tracker.latest_message["entities"]:
					entity_name = entity.get("entity")
					entity_value = entity.get("value")
			apiresponse = fetch_faq_response_api(GROUP_ID,intent_name,entity_name,entity_value,platform,sender_id)
			logging.info(apiresponse)
			formatted_response = format_api_response(apiresponse)  
			dispatcher.utter_message(json.dumps(formatted_response))
			return [UserUtteranceReverted()]
		except BaseException as e:
			if os.path.isdir("exceptions") == False:
				os.mkdir('exceptions')
			
			dateTimeObj = datetime.now()
			timestampStr = dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S.%f)")
			file_name = intent_name + '_' + timestampStr + '.txt'
			f = open('exceptions/'+file_name, "a+")
			f.write(str(e))
			f.write(str(tracker.events))
			f.close()
			ssml = "<s>I am sorry! I am still learning and I could not understand what you said. Could you please say that again?</s>"
			fallback_message = '[{ "message": { "template": { "elements": { "title": "I am sorry! I am still learning and I could not understand what you said. Could you please say that again?", "says": "", "visemes": ""},"type": "Card"} } },{"ssml": "<speak>' + ssml + '</speak>"}]'
			dispatcher.utter_message(fallback_message)
			return [UserUtteranceReverted()]
